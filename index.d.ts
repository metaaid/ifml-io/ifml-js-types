
declare module "ifml-js" {
    class Viewer {
        importXML(xml: string): void;
    }
    class Modeler {
        constructor(options: Object);
        importXML(xml: string): Promise<void>;
        createDiagram(): Promise<void>;
    }
}